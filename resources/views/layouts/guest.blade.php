<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A powerful and conceptual apps base dashboard template that especially build for developers and programmers.">
    <!-- Fav Icon  -->

    <!-- Page Title  -->
    <title>Blank Layout | DashLite Admin Template</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{ mix('backend/css/dashlite.css') }}">
    <link id="skin-default" rel="stylesheet" href="{{ mix('backend/css/theme.css') }}">
</head>

<body class="nk-body npc-default pg-auth">
<div class="nk-app-root">
    <!-- main @s -->
    <div class="nk-main ">
        <!-- wrap @s -->
        <div class="nk-wrap nk-wrap-nosidebar">
            <!-- content @s -->
            <div class="nk-content ">
                @yield('content')
                <div class="nk-footer nk-auth-footer-full">
                    <div class="container wide-lg">
                        <div class="row g-3">
                            <div class="col-lg-6 order-lg-last">
                                <ul class="nav nav-sm justify-content-center justify-content-lg-end">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Terms & Condition</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Privacy Policy</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Help</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <div class="nk-block-content text-center text-lg-left">
                                    <p class="text-soft">&copy; 2022 Matthens Design School. All Rights Reserved.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- content @e -->
    </div>
    <!-- main @e -->
</div>
<!-- app-root @e -->
<!-- JavaScript -->
<!-- Footer End -->

<!-- Back to top -->
<a href="#" onclick="topFunction()" id="back-to-top" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
<!-- Back to top -->

<script src="{{ asset('assets/js/bootstrap.bundle.min.js')  }}"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>

<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<script type="text/javascript">
    $('.js-slick').slick({

        slidesToShow: 1,
        fade: true,
        autoplay: true,
        autoplaySpeed: 1000,
        dots: true,
        infinite: true,
        arrows: true

    });

    $(window).load(function(){setTimeout(function(){$('.loader-container').addClass('active')},1000)})
    var x=document.getElementById("myAudio");function playAudio(){x.play()}$(document).ready(function(){$(".js-hamburger-cta").on("click",function(a){return playAudio(),$(".full-nav").toggleClass("js-active"),$("#hamburger-6").toggleClass("js-active"),$(".navbar-brand, #navigation, .breadcrumb--custom, .brand-name").toggleClass("js-hide"),$(".main-navbar").toggleClass("js-hide-bg"),$(".hamburger-cta .line").toggleClass("js-color"),$(".nav-left-block").toggleClass("js-transition"),$(".nav-right-block").toggleClass("js-transition"),$(".nav-media-block").toggleClass("js-transition"),$(".full-nav").animate({scrollTop:0},"fast"),!1}),$(window).on("load resize",function(){window.matchMedia("(min-width: 1100px)").matches&&($(".nav-primary-list a").mouseover(function(){$(".nav-primary-list a").css({color:"rgba(255, 255, 255, 0.4",transition:"all cubic-bezier(0.25, 0.1, 0.39, 0.58) .45s"}),$(this).css({color:"#fff",transition:"all cubic-bezier(0.25, 0.1, 0.39, 0.58) .45s"})}),$(".nav-primary-list a").mouseout(function(){$(".nav-primary-list a").css({color:"#fff",transition:"all cubic-bezier(0.25, 0.1, 0.39, 0.58) .45s"})}))})});


</script>
<!-- SLIDER -->
<script src="{{ asset('assets/js/tiny-slider.js') }}  "></script>
<!-- Icons -->
<script src="{{ asset('assets/js/feather.min.js') }}"></script>
<!-- Main Js -->
<script src="{{ asset('assets/js/plugins.init.js') }}"></script><!--Note: All init js like tiny slider, counter, countdown, maintenance, lightbox, gallery, swiper slider, aos animation etc.-->
<script src="{{ asset('assets/js/app.js') }}"></script><!--Note: All important javascript like page loader, menu, sticky menu, menu-toggler, one page menu etc. -->
<!-- select region modal -->


</html>
