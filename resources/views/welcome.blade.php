
@extends('layouts.app')
@section('content')
<!-- Hero Start -->
<section class="bg-half-80 banner d-table w-100 "  style="background: url('assets/images/banner-texture.gif') repeat-x;background-position: 100% 100%; overflow: hidden;">
    <div class="container">
        <div class="row mt-5 align-items-center text-left">
            <div class="col-lg-8 col-md-8">
                <div class="title-heading me-lg-4 ">
                    <h1 class="heading fnt-neuton ">Become a UI Design Genius</h1>
                    <p class="para-desc">Become a UX designer, UI designer or web developer from scratch. Learn what industry wants with an expert led classroom training.</p>
                    <div class="mt-4">
                        <a href="javascript:void(0)" class="btn btn-lg btn-blue mt-2 me-2">Talk to us</a>
                        <a href="javascript:void(0)" class="btn btn-lg btn-outline-light mt-2 me-2">Enroll Now</a>
                    </div>

                </div>
            </div><!--end col-->
            <div class="col-lg-4 col-md-4 ">
                <img src="assets/images/homeHero.png" class="img-fluid  " alt="">
            </div>

        </div>
    </div><!--end container-->
</section><!--end section-->

<!-- Courses Start -->
<section class="section" id="courses">
    <div class="container">


        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                <a href="{{ url('courses/ux-ui-design-essential-course ') }}">
                    <div class="card courses-desc overflow-hidden rounded shadow border-0">
                        <div class="position-relative d-block overflow-hidden">
                            <img src="{{ asset('assets/images/preview.jpg') }}" class="img-fluid rounded-top mx-auto" alt="">
                            <div class="overlay-work bg-dark"></div>
                            <a href="javascript:void(0)" class="text-white h6 preview">Malayalam <i class="uil uil-angle-right-b align-middle"></i></a>
                        </div>

                        <div class="card-body">
                            <h5><a href="javascript:void(0)" class="title text-dark">Learn Figma : UI UX Design Essential Training  </a></h5>
                            <p>This 15 Days UX / UI course prepares freshers to design the next generation mobile apps, websites, and digital products.</p>
                            <div class="fees d-flex justify-content-between">
                                <ul class="list-unstyled mb-0 numbers">
                                    <li><i class="uil uil-graduation-cap text-muted"></i> 14 days, 21 sessions, 20 hours</li>
                                    <li><i class="uil uil-graduation-cap text-muted"></i> Next Batch : 26 Jan 2022</li>
                                </ul>
                                <h4><span class="h6">₹</span>2499</h4>
                            </div>
                            <div class="mt-4">

                                <a href="course-info.php" class="btn btn-outline-light btn-lg btn-block">Enroll Next Batch</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div><!--end col-->
            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="assets/images/ux-course-kochi.png" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="javascript:void(0)" class="text-white h6 preview">Malayalam <i class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="javascript:void(0)" class="title text-dark">UI UX Master Course  </a></h5>
                        <p>We have crafted a 10 - weeks course that will enable you to transform into a designer. </p>
                        <div class="fees d-flex justify-content-between">
                            <ul class="list-unstyled mb-0 numbers">
                                <li><i class="uil uil-graduation-cap text-muted"></i> 12 - weeks course </li>
                                <li><i class="uil uil-graduation-cap text-muted"></i> Next Batch : March 2022</li>
                            </ul>
                            <h4><span class="h6">₹</span>74,700*</h4>
                        </div>
                        <div class="mt-4">
                            <a href="javascript:void(0)" class="btn btn-outline-light btn-lg btn-block">Request a Call back</a>

                        </div>
                    </div>
                </div>
            </div><!--end col-->






        </div><!--end row-->
    </div><!--end container-->


</section><!--end section-->
<!-- Courses End -->
<!-- How It Work Start -->
<section class="student-portfolio  " >
    <div class="h-100">
        <div class="js-slick project-list">
            <div style="background:#00414e">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 order-1 order-md-2">
                            <img src="assets/images/fitness_banner@2x.png" class="img-fluid" alt="">
                        </div><!--end col-->

                        <div class="col-lg-6 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
                            <div class="section-title me-lg-5">
                                <p class="badge-outline-light text-white">STUDENT PROJECTS</p>
                                <h2 class="mb-4  text-white ">FitBook Fitness App</h4>
                                    <p class=" fnt-neuton text-white  sub-title " >FitBook is a Premium and High-Quality Fitness & Workout App UI Desiged in Figma.  </p>


                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div>
            </div>
            <div style="background:#ecf5fc">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 order-1 order-md-2">
                            <img src="assets/images/healthcare_banner@2x.png" class="img-fluid" alt="">
                        </div><!--end col-->

                        <div class="col-lg-6 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
                            <div class="section-title me-lg-5">
                                <p class="badge-outline-light ">STUDENT PROJECTS</p>
                                <h2 class="  mb-4 ">DocFind - Doctor Booking App </h4>
                                    <p class=" fnt-neuton sub-title " >DocFind is a Doctor Consultation App, this app helps patients to book their consultation with the doctor. </p>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div>
            </div>
            <div style="background:#fee4cd" >
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 order-1 order-md-2">
                            <img src="assets/images/fashion_banner@2x.png" class="img-fluid" alt="">
                        </div><!--end col-->

                        <div class="col-lg-6 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
                            <div class="section-title me-lg-5">
                                <p class="badge-outline-light ">STUDENT PROJECTS</p>
                                <h2 class="  mb-4 ">Outfit - Fashion App </h4>
                                    <p class=" fnt-neuton sub-title " >Did you know you can pay a stylist so he (or she) will pick up a wardrobe to match certain clothes that you already have and love? Concept App </p>

                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div>
            </div>
            <div style="background:#0c316a" >
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 order-1 order-md-2">
                            <img src="assets/images/streamapp_banner@2x.png" class="img-fluid" alt="">
                        </div><!--end col-->

                        <div class="col-lg-6 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
                            <div class="section-title me-lg-5">
                                <p class="badge-outline-light text-white">STUDENT PROJECTS</p>
                                <h2 class="  text-white   mb-4 ">dStream - Streaming App </h4>
                                    <p class=" text-white fnt-neuton sub-title " > dStream is the best free video live streaming app for creators. Play mobile games and stream your screen or broadcast your camera to social platforms such as Twitch, YouTube, Facebook, and more!</p>

                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div>
            </div>
        </div>
    </div>
</section><!--end section-->
<!-- How It Work End -->
<!-- Hero Start -->
<section class="section">
    <div class="container">

        <div class="row mt-5 align-items-center ">
            <div class="col-12 text-center">
                <h2 class="title mb-4">Making expectations a reality</h3>
                    <p class="text-muted  sub-title mx-auto"> Regardless of where you work and what job title you hold, raising aspirations for design thinking and creative skills are now more than ever. Hence we built a bridge for you to transition into a designer.</p>
                </h2>
            </div>
        </div><!--end container-->

        <div class="row mt-5 align-items-center text-left">
            <div class="col-md-4 col-12  mt-5">
                <div class="features pt-4 pb-4">
                    <div class="icon">
                        <img src="assets/images/handson-icon.svg" class="img-fluid">
                    </div>

                    <div class="content mt-4">
                        <h3 class="fnt-neuton">Hands-on training</h5>
                            <p class="para-desc mb-0">Fully immersive, hands-on experience of building a concept from scratch.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12  mt-5">
                <div class="features pt-4 pb-4">
                    <div class="icon">
                        <img src="assets/images/mentorship_icon.svg" class="img-fluid">
                    </div>

                    <div class="content mt-4">
                        <h3 class="fnt-neuton">1-on-1 Mentorship</h5>
                            <p class="para-desc mb-0">Work with our leading experts and gain access to personalized guidance to reach your potential.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12  mt-5">
                <div class="features pt-4 pb-4">
                    <div class="icon">
                        <img src="assets/images/community_icon.svg" class="img-fluid">
                    </div>

                    <div class="content mt-4">
                        <h3 class="fnt-neuton">Community Access</h5>
                            <p class="para-desc mb-0">Expert videos are added regularly to include insights from industry leaders</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12  mt-5">
                <div class="features pt-4 pb-4">
                    <div class="icon">
                        <img src="assets/images/learning_icon.svg" class="img-fluid">
                    </div>

                    <div class="content mt-4">
                        <h3 class="fnt-neuton">Learn By Doing</h5>
                            <p class="para-desc mb-0">Handy assignments to re-inforce the learning</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12  mt-5">
                <div class="features pt-4 pb-4">
                    <div class="icon">
                        <img src="assets/images/live_icon.svg" class="img-fluid">
                    </div>

                    <div class="content mt-4">
                        <h3 class="fnt-neuton">Live Classes</h5>
                            <p class="para-desc mb-0">Learn Skills with a bit of ❤️, directly from Industry Experts</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12  mt-5">
                <div class="features pt-4 pb-4">
                    <div class="icon">
                        <img src="assets/images/dreamjob_icon.svg" class="img-fluid">
                    </div>

                    <div class="content mt-4">
                        <h3 class="fnt-neuton">Get Your Dream Job</h3>
                        <p class="para-desc mb-0">It is never too late to start preparing. Maybe you dream job is just a decision away.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5 align-items-center text-left">
            <div class="col-lg-7 col-md-8">
                <div class=" me-lg-4 ">
                    <h3 class=" fnt-neuton ">Get Certified</h3>
                    <p class="para-desc">We provide pro certification after the course completion. Valued by all the Design/Tech industries out there.</p>
                    <div class="mt-4">
                        <a href="javascript:void(0)" class="btn btn-outline-light mt-2 me-2">Join Next Batch</a>
                    </div>
                </div>
            </div><!--end col-->
            <div class="col-lg-5 col-md-4 ">
                <img src="assets/images/preview-certificate.png" class="img-fluid  " alt="">
            </div>

        </div>


</section><!--end section-->

<!-- Testi Start -->
<section class="section ">
    <div class="container mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title">
                    <h4 class="title mb-4">Experience the new way of learning </h4>
                    <p class="text-muted  sub-title mx-auto">Join  Matthens  and build the skills need to take on a changing world.

                    </p>

                    <div class="mt-4">
                        <a href="https://wa.me/message/5FIEQWEZIUONE1" target="_blank" rel="noopener noreferrer"><img class="whatsapp" src="{{ asset('assets/images/Button_Whatsapp.svg') }}" alt=""></a>

                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->

</section><!--end section-->
<!-- Testi End -->
<!-- FAQ n Contact End -->


@endsection
