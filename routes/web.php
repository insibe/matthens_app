<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/account', function () {
    return view('account');
});




Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('company/matthens-design-academy', function () {
    return view('about');
});
Route::get('company/stories', function () {
    return view('stories');
});
Route::get('company/media-kit', function () {
    return view('media-kit');
});
Route::get('company/contact-us', function () {
    return view('contact');
});

Route::get('legal/cancellation-and-refund-policy', function () {
    return view('privacy-policy');
});


Route::get('legal/terms-and-conditions', function () {
    return view('terms-conditions');
});

Route::get('legal/terms-of-service', function () {
    return view('terms-service');
});

Route::get('legal/cookie-policy', function () {
    return view('cookie-policy');
});


Route::get('legal/privacy-policy', function () {
    return view('privacy-policy');
});

Route::get('legal/content-policy', function () {
    return view('content-policy');
});


Route::get('/courses/ui-ux-design-essential-training', function () {
    return view('course-single');
});
Route::get('/courses/ux-ui-demo-class', function () {
    return view('course-demo');
});



Route::get('/dashboard', function () {
    return view('dashboard.index');
});



Route::get('/membership-central', function () {
    return view('users.index');
});


//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    return '<h1>View cache cleared</h1>';
});











Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
